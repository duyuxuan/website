# Dump of table club_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `club_category`;

CREATE TABLE `club_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '分类创建的用户ID',
  `title` varchar(512) DEFAULT NULL COMMENT '标题',
  `summary` text COMMENT '内容描述',
  `icon` varchar(128) DEFAULT NULL COMMENT '图标',
  `count` int(11) unsigned DEFAULT '0' COMMENT '该分类的内容数量',
  `order_number` int(11) DEFAULT '0' COMMENT '排序编码',
  `flag` varchar(256) DEFAULT NULL COMMENT '标识',
  `meta_keywords` varchar(256) DEFAULT NULL COMMENT 'SEO关键字',
  `meta_description` varchar(256) DEFAULT NULL COMMENT 'SEO描述内容',
  `created` datetime DEFAULT NULL COMMENT '创建日期',
  `modified` datetime DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `order_number` (`order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社区帖子分类';



# Dump of table club_member_record
# ------------------------------------------------------------

DROP TABLE IF EXISTS `club_member_record`;

CREATE TABLE `club_member_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `create_source` varchar(32) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `duetime` datetime DEFAULT NULL,
  `remark` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_and_type` (`user_id`,`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table club_member_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `club_member_type`;

CREATE TABLE `club_member_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `icon` varchar(256) DEFAULT NULL,
  `link` varchar(256) DEFAULT NULL,
  `flag` varchar(32) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table club_post
# ------------------------------------------------------------

DROP TABLE IF EXISTS `club_post`;

CREATE TABLE `club_post` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `content` text,
  `status` varchar(32) DEFAULT NULL,
  `comment_enable` tinyint(1) DEFAULT NULL,
  `comment_count` int(10) unsigned DEFAULT '0',
  `comment_time` datetime DEFAULT NULL,
  `view_count` int(10) unsigned DEFAULT '0' COMMENT '浏览量',
  `title_bold` int(11) DEFAULT NULL COMMENT '标题加粗',
  `title_color` varchar(32) DEFAULT NULL COMMENT '标题颜色',
  `top_level` int(11) DEFAULT '0' COMMENT '置顶级别',
  `adopt` tinyint(1) DEFAULT NULL COMMENT '是否已经采纳',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社区帖子表';



# Dump of table club_post_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `club_post_comment`;

CREATE TABLE `club_post_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `pid` int(11) unsigned DEFAULT NULL COMMENT 'pid',
  `post_id` int(11) unsigned DEFAULT NULL COMMENT '评论的内容ID',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '评论的用户ID',
  `content` text COMMENT '评论的内容',
  `reply_count` int(11) unsigned DEFAULT '0' COMMENT '评论的回复数量',
  `order_number` int(11) unsigned DEFAULT '0' COMMENT '排序编号，常用语置顶等',
  `vote_up` int(11) unsigned DEFAULT '0' COMMENT '“顶”的数量',
  `vote_down` int(11) unsigned DEFAULT '0' COMMENT '“踩”的数量',
  `adopt` tinyint(1) DEFAULT NULL COMMENT '是否被采纳',
  `status` varchar(32) DEFAULT NULL COMMENT '评论的状态',
  `created` datetime DEFAULT NULL COMMENT '评论的时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='评论表';

