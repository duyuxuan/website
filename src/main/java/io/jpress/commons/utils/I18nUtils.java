package io.jpress.commons.utils;

import com.jfinal.i18n.I18n;
import com.jfinal.i18n.Res;
import com.jfinal.kit.StrKit;

import io.jboot.web.controller.JbootControllerContext;
import io.jpress.JPressConsts;

public class I18nUtils {
	
	public static Res getRes(){
		try {
			String locale = JbootControllerContext.get().getCookie(JPressConsts.COOKIE_LOCALE_PARA_NAME);
			if(StrKit.notBlank(locale)){
				return I18n.use(locale);
			}else{
				return I18n.use(JPressConsts.COOKIE_I18N_DEFAULT_NAME);
			}
		} catch (NullPointerException e){
			return I18n.use(JPressConsts.COOKIE_I18N_DEFAULT_NAME);
		}
		
	}
	
	public static String convert(String key){
		return getRes().get(key);
	}
	
	public static String getSuffix() {
		String _locale = "_" + JPressConsts.COOKIE_I18N_DEFAULT_NAME.substring(0,2);
		try {
			String locale = JbootControllerContext.get().getCookie(JPressConsts.COOKIE_LOCALE_PARA_NAME);
			if(StrKit.notBlank(locale)){
				_locale = "_" + locale.substring(0,2);
			}
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return _locale;
	}
	
	public static boolean hasSuffix(String path) {
		if (path.indexOf("_ja") > -1 || path.indexOf("_zh") > -1) {
    		return true;
    	}
		return false;
	}
	
	public static String getUrlNoSuffix(String url) {
		if (hasSuffix(url) && url.length() >= 3) {
			url = url.substring(0, url.length()-3);
		}
		return url;
	}
}
