package io.jpress.web.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.config.Routes;
import com.jfinal.core.Action;
import com.jfinal.core.ActionMapping;
import com.jfinal.core.Const;
import com.jfinal.core.Controller;
import com.jfinal.i18n.I18n;
import com.jfinal.i18n.Res;
import com.jfinal.kit.StrKit;
import com.jfinal.render.Render;

import io.jpress.JPressConsts;
import io.jpress.core.menu.MenuManager;
import net.sf.ehcache.util.concurrent.ConcurrentHashMap;;

public class MyI18nInterceptor implements Interceptor {

	private String localeParaName = JPressConsts.COOKIE_LOCALE_PARA_NAME;
	private String resName = "_res";
	private boolean isSwitchView = false;
	private static Routes ucRoutes = new UserCenterRoutes();
	private static UserCenterMapping actionMapping = new UserCenterMapping(ucRoutes);
	
	public MyI18nInterceptor() {
	}
	
	public MyI18nInterceptor(String localeParaName, String resName) {
		if (StrKit.isBlank(localeParaName)) {
			throw new IllegalArgumentException("localeParaName can not be blank.");
		}
		if (StrKit.isBlank(resName)) {
			throw new IllegalArgumentException("resName can not be blank.");
		}
		
		this.localeParaName = localeParaName;
		this.resName = resName;
	}
	
	public MyI18nInterceptor(String localeParaName, String resName, boolean isSwitchView) {
		this(localeParaName, resName);
		this.isSwitchView = isSwitchView;
	}
	
	public MyI18nInterceptor(boolean isSwitchView) {
		this.isSwitchView = isSwitchView;
	}
	
	/**
	 * Return the localeParaName, which is used as para name to get locale from the request para and the cookie.
	 */
	protected String getLocaleParaName() {
		return localeParaName;
	}
	
	/**
	 * Return the resName, which is used as attribute name to pass the Res object to the view.
	 */
	protected String getResName() {
		return resName;
	}
	
	/**
	 * Return the baseName, which is used as base name of the i18n resource file.
	 */
	protected String getBaseName() {
		return JPressConsts.COOKIE_I18N_BASE_NAME;
	}
	
	/**
	 * 1: use the locale from request para if exists. change the locale write to the cookie
	 * 2: use the locale from cookie para if exists.
	 * 3: use the default locale
	 * 4: use setAttr(resName, resObject) pass Res object to the view.
	 */
	public void intercept(Invocation inv) {
		Controller c = inv.getController();
		String localeParaName = getLocaleParaName();
		String locale = c.getPara(localeParaName);
		
		if (StrKit.notBlank(locale)) {	// change locale, write cookie
			c.setCookie(localeParaName, locale, Const.DEFAULT_I18N_MAX_AGE_OF_COOKIE);
		}
		else {							// get locale from cookie and use the default locale if it is null
			locale = c.getCookie(localeParaName);
			if (StrKit.isBlank(locale)) {
				locale = JPressConsts.COOKIE_I18N_DEFAULT_NAME;
				c.setCookie(localeParaName, locale, Const.DEFAULT_I18N_MAX_AGE_OF_COOKIE);
			}
		}
		
		inv.invoke();
		
		if (isSwitchView) {
			switchView(locale, c);
		}
		Res res = I18n.use(getBaseName(), locale);
		c.setAttr(getResName(), res);
	}
	
	/**
	 * 在有些 web 系统中，页面需要国际化的文本过多，并且 css 以及 html 也因为际化而大不相同，
	 * 对于这种应用场景先直接制做多套同名称的国际化视图，并将这些视图以 locale 为子目录分类存放，
	 * 最后使用本拦截器根据 locale 动态切换视图，而不必对视图中的文本逐个进行国际化切换，省时省力。
	 */
	public void switchView(String locale, Controller c) {
		Render render = c.getRender();
		if (render != null) {
			String view = render.getView();
			if (view != null) {
				if (view.startsWith("/templates/")) {
					view = addPath(view, locale);
				} 
//				else if (c.getControllerKey().equals("/ucenter")){
//			        String viewPath = "/WEB-INF/views/ucenter/" + locale + "/";
//			        view = viewPath + view;
//				} 
				render.setView(view);
			}
		}
	}
	
	private String addPath(String view, String locale) {
		String[] url = view.split("/");
		int len = url.length;
		String new_url = "";
		for (int i = 0; i < len-1; i++){ 
			new_url += url[i];
			new_url += "/";
		}
		new_url += locale + "/";
		new_url += url[len-1];
		view = new_url;
		return view;
	}
	
	public static class UserCenterMapping extends ActionMapping {

	    public UserCenterMapping(Routes routes) {
	        super(routes);
	        this.mapping = new ConcurrentHashMap<>();
	    }

	    @Override
	    public void buildActionMapping() {
	        super.buildActionMapping();
	    }


	    @Override
	    public Action getAction(String url, String[] urlPara) {
	        return super.getAction(url, urlPara);
	    }
	}
	
	public static class UserCenterRoutes extends Routes {

        public UserCenterRoutes() {
            //setClearAfterMapping(false) 不让 AddonActionMapping 在构建完毕后对已经添加的 Routes 进行清除的工作
            setClearAfterMapping(false);

        }

        @Override
        public void config() {
            //do nothing
        }
    }

}
