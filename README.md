## warファイル作成(Tomcat向け)
mvn clean package -f pom-war.xml

## Azure デプロイ（PowerShellで）
Connect-AzAccount

Publish-AzWebapp -ResourceGroupName website -Name tcsc-site -ArchivePath E:\tcsc-dev\gitlab\website\target\website-0.0.1.war


## 参考リンク
- [herokuコマンド一覧](https://devcenter.heroku.com/articles/heroku-cli-commands)
- [warファイルでデプロイ](https://devcenter.heroku.com/articles/war-deployment)
- [PowerShellでwarファイルをデプロイ](https://docs.microsoft.com/ja-jp/azure/app-service/deploy-zip#with-powershell)